

import java.util.*;
public class Xogame {
	
	public static char[][] table = new char[3][3];
	public static Scanner kb = new Scanner(System.in);
	public static int row=0;
	public static int col=0;
	public static int sum = 0;
	public static int checkendgame = 0;
	public static int roww = 0;
	public static int coll = 0;
	public static int round = 0;
	
	
	public static void main(String[] args) {
		
		ShowSay();
		ShowTable(table);
		Input();
		CheckCompetition();
	}

	public static void CheckCompetition(){
		while(checkendgame == 0) {
			InputO();
			Input();
			checkOwin();
			checkDraw();
		  if(checkendgame > 0) {
			  break;
		  }
			InputX();
			Input();
			checkXwin();
			checkDraw();
			if(checkendgame > 0) {
				  break;
			  }
	}
		printOwin();
	    printXwin();
		printDraw();
		showBye();
	}
	public static void ShowSay() {
		System.out.println("----- Welcome to game -----");
	}
	
	public static void ShowTable(char[][] table) {
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				table[i][j] = '_';
			}
		}
	}
	public static void Input() {
        for (int k = 0 ; k < 3 ; k++){
            for (int l = 0 ; l < 3 ; l++){
                System.out.print(table[k][l]+ " | " );
            }
            System.out.println();
        }
	}
	public static void showInput(){
		System.out.print("Please input row and collum :  ");
	}
	public static void Inputrowcol() {
			showInput();
	        row = kb.nextInt();
	        col = kb.nextInt();
	    }
	   
	    public static void InputO() {
	        System.out.println("turn O");
			Inputrowcol();
			CheckColO();
			CheckRowColO();
	        CheckEmptyO();   
		}
		public static void showError(){
			System.out.println("input must in range (0 - 2 )!!");
		}
		public static void CheckColO(){
			if ((col > 2 || col < 0) && col < 2) {
	        	showError();
	            InputO();
	        }
		}
		public static void CheckRowColO(){
			if ((row > 2 || row < 0) && col < 2) {
	        	showError();
				InputO();
	        } else  if(col > 2 && row > 2){
	        	showError();
	            InputO();
	        }
		}
		public static void showMessage(){
			System.out.println("This position is already filled etc.");
		}
		public static void CheckEmptyO(){
			if (table[row][col]==('O') || table[row][col]==('X')) {
	        	showMessage();
	            InputO();
	        } else {
	            table[row][col] = ('O');
	            sum++;
	        }
		}
		
	    public static void InputX() {
	        System.out.println("turn X");
			Inputrowcol();
			checkRowX();
			checkColX();
	        CheckEmptyX();
		}
		public static void checkRowX(){
			if (row > 2 || row < 0) {
	        	showError();
	            InputX();
	        }
		}
		public static void checkColX(){
			if (col > 2 || col < 0) {
	        	showError();
	            InputX();
	        } else if(col > 2 && row > 2){
	        	showError();
	            InputX();
	        }
		}
		public static void CheckEmptyX(){
			if (table[row][col]==('O') || table[row][col]==('X')) {
	        	showMessage();
	            InputX();
	        } else {
	            table[row][col] = ('X');
	            sum++;
	        }   
		}

	    public static void checkOwin() {
	        if(table[roww][coll]==('O') && table[roww][coll + 1]==('O') && table[roww][coll + 2]==('O')
	            || table[roww][coll]==('O') && table[roww + 1][coll]==('O') && table[roww + 2][coll]==('O')
	            || table[roww][coll]==('O') && table[roww + 1][coll + 1]==('O')&& table[roww + 2][coll + 2]==('O')
	            || table[roww + 1][coll]==('O') && table[roww + 1][coll + 1]==('O')&& table[roww + 1][coll + 2]==('O')
	            || table[roww + 2][coll]==('O') && table[roww + 2][coll + 1]==('O')&& table[roww + 2][coll + 2]==('O')
	            || table[roww][coll + 1]==('O') && table[roww + 1][coll + 1]==('O')&& table[roww + 2][coll + 1]==('O')
	            || table[roww][coll + 2]==('O') && table[roww + 1][coll + 2]==('O')&& table[roww + 2][coll + 2]==('O')
	            || table[roww][coll + 2]==('O') && table[roww + 1][coll + 1]==('O')&& table[roww + 2][coll]==('O')){
	            checkendgame = 1; 
	        }       
		}
		
	    public static void checkXwin () {
	        if(table[roww][coll]==('X') && table[roww][coll + 1]==('X') && table[roww][coll + 2]==('X')
	            || table[roww][coll]==('X') && table[roww + 1][coll]==('X') && table[roww + 2][coll]==('X')
	            || table[roww][coll]==('X') && table[roww + 1][coll + 1]==('X')&& table[roww + 2][coll + 2]==('X')
	            || table[roww + 1][coll]==('X') && table[roww + 1][coll + 1]==('X')&& table[roww + 1][coll + 2]==('X')
	            || table[roww + 2][coll] == ('X') && table[roww + 2][coll + 1]==('X')&& table[roww + 2][coll + 2]==('X')
	            || table[roww][coll + 1]==('X') && table[roww + 1][coll + 1]==('X')&& table[roww + 2][coll + 1]==('X')
	            || table[roww][coll + 2]==('X') && table[roww + 1][coll + 2]==('X')&& table[roww + 2][coll + 2]==('X')
	            || table[roww][coll + 2]==('X') && table[roww + 1][coll + 1]==('X')&& table[roww + 2][coll]==('X')){
	            checkendgame = 2;
	        }
	    }
	    public static void checkDraw () {
	        if(sum == 9 && checkendgame == 0) {
	            checkendgame = 3;
	        }      
	    }
	    public static void printOwin () {
	        if(checkendgame == 1){
	            System.out.println("O Win!!!");
		}
	    }
	    public static void printXwin () {
	        if(checkendgame == 2){ 
	           System.out.println("X Win!!!");
		}
	    }
	    public static void printDraw () {
	        if (checkendgame == 3) {
	            System.out.println("Draw");
		}
		}
		public static void showBye(){
			System.out.println("Bye bye...");
		}
		
	    
	    
}

