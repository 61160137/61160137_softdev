import java.util.Random;

public class Table {
	private char[][] data = {{'_','_','_'},{'_','_','_'},{'_','_','_'}};
	private Player currentPlayer;
	private Player o;
	private Player x;
	private Player win;
	
	public Table(Player x1, Player o1) {
		x = x1;
		o = o1;
		Random rand = new Random();
		if (rand.nextInt(2) + 1 == 1) {
			currentPlayer = o;
		} else {
			currentPlayer = x;
		}
	}


	public void setRowCol(int row,int col) throws Exception  {
		if (this.getTable()[row][col] == '_') {
			data[row][col] = currentPlayer.getName();
		} else {
			throw new Exception();
		}
	}
	public boolean checkWin() {
		if (checkX1() || checkX2() || checkRow()||checkCol()) {
			win = currentPlayer;
			if (currentPlayer == x) {
				x.Win();
				o.Lose();
			} else {
				o.Win();
				x.Lose();
			} 
			return true;
		}
		return false;
	}
	public boolean checkDraw() {
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data.length; j++) {
				if (data[i][j] != '_') {
					count++;
				}
			}
		}
		if (count == 9 && checkWin() != true) {
			x.Draw();
			o.Draw();
			return true;
		}
		return false;
	}

	
	public void swithTurn() {
		if (currentPlayer == x) {
			currentPlayer = o;
		} else {
			currentPlayer = x;
		}

	}
	public Player getCurretPlayer() {
		return currentPlayer;
	
	}
	public Player getWinPlayer() {
		return win;
	}

	
	public char[][] getTable() {
		return data;
	}
public boolean checkRow() {
	 if(data[0][0]=='O'&&data[0][1]=='O'&&data[0][2]=='O'
		||data[1][0]=='O'&&data[1][1]=='O'&&data[1][2]=='O'
		||data[2][0]=='O'&&data[2][1]=='O'&&data[2][2]=='O') {
		 return true;
	 }else if (data[0][0]=='X'&&data[0][1]=='X'&&data[0][2]=='X'
				||data[1][0]=='X'&&data[1][1]=='X'&&data[1][2]=='X'
				||data[2][0]=='X'&&data[2][1]=='X'&&data[2][2]=='X') {
		 return true;
	 }
	 return false;
	}
public boolean checkCol() {
		if(data[0][0]=='O'&&data[1][0]=='O'&&data[2][0]=='O'
				||data[0][1]=='O'&&data[1][1]=='O'&&data[2][1]=='O'
				||data[0][2]=='O'&&data[1][2]=='O'&&data[2][2]=='O') {
				 return true;
			 }else if (data[0][0]=='X'&&data[1][0]=='X'&&data[2][0]=='X'
						||data[0][1]=='X'&&data[1][1]=='X'&&data[2][1]=='X'
						||data[0][2]=='X'&&data[1][2]=='X'&&data[2][2]=='X') {
				 return true;
			 }
			 return false;
	}
public boolean checkX1() {
		if(data[0][0]=='O'&&data[1][1]=='O'&&data[2][2]=='O') {
			return true;
		}else if (data[0][0]=='X'&&data[1][1]=='X'&&data[2][2]=='X') {
			return true;
		}
		return false;
	}
public boolean checkX2() {
		if(data[2][0]=='O'&&data[1][1]=='O'&&data[0][2]=='O') {
			return true;
		}else if(data[2][0]=='X'&&data[1][1]=='X'&&data[0][2]=='X') {
			return true;
		}
		return false;
	}
}