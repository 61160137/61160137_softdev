import java.util.*;
public class Game {
	private Table table;
	private int row;
	private int col;
	private Player o;
	private Player x;
Scanner kb = new Scanner(System.in);
public Game() {
	x = new Player('X');
	o = new Player('O');

}
public void startGame() {
	table = new Table(x, o);
}
public void showWelcome() {
	System.out.println("----- Welcome to game -----");
}
public void showTable() {
	for(int i=0;i<3;i++) {
		for(int j=0;j<3;j++) {
			System.out.print(table.getTable()[i][j]+" ");
		}
		System.out.println();
	}
}
public void showTurn() {
	System.out.println("Turn  " + table.getCurretPlayer().getName());
}
public void inputRowCol() {
	System.out.print("Please input row and collum :  ");
	try {
		row = kb.nextInt();
		col = kb.nextInt();
		table.setRowCol(row, col);
	} catch (Exception e) {
		System.out.println("input must in range (0 - 2 )!!");
		kb.nextLine();
		this.inputRowCol();
	}
}
public boolean inputContinue() {
	System.out.print("Do you want to continue playing? (y/n) : ");
	char check = kb.next().charAt(0);
	do {
		if (check != 'y' && check != 'n') {
			System.out.print("Do you want to continue playing? (y/n) : ");
			check = kb.next().charAt(0);
		} else if (check == 'y'|| check == 'Y') {
			return true;
		}
		return false;
	} while (true);
}
public void rungame() {
	this.startGame();
	this.showWelcome();
	do {
		this.showTable();
		this.showTurn();
		this.run();
		this.startGame();
	} while (this.inputContinue());
	this.showBye();
}
public void run() {
	while (true) {
		this.inputRowCol();
		this.showTable();
		if (table.checkWin()) {
			this.showWin();
			break;
		} else if (table.checkDraw()) {
			this.showDraw();
			break;
		}
		table.swithTurn();
		this.showTurn();
	}
}
public void showDraw() {
	System.out.println("Player X and O is Draw");
}

public void showWin() {
	System.out.println("Player " + table.getWinPlayer().getName() + " is WIN!!");
}


public void showBye() {
	System.out.println("Bye bye....");
}
}
